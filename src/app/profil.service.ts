import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProfilService {

  constructor(private http:HttpClient) { }
  apiUrl="http://localhost:5000/client/"
  ajouterClient(clientData): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}form`, clientData);
  }
  getClient(): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}`);
  }
}
