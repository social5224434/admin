import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialogActions, MatDialogContent, MatDialog } from '@angular/material/dialog';
import { TableListComponent } from '../table-list.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';


@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css'],
  // standalone: true,

 
})

export class DialogComponent implements OnInit {
  
  clients: any[]; // Ajoutez une propriété pour stocker les données clients

  constructor(public dialogRef: MatDialogRef<DialogComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit(): void {
    this.clients = this.data.client; // Initialisez la propriété avec les données transmises
    console.log("this", this.data);
    console.log("this",this.clients);
    
  }

}
