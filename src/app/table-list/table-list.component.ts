import { AfterViewChecked, AfterViewInit, Component, Inject, OnInit } from '@angular/core';
import { ProfilService } from 'app/profil.service';
import { BehaviorSubject } from 'rxjs';
import {
  MatDialog,
  MAT_DIALOG_DATA,
  MatDialogRef,
  MatDialogTitle,
  MatDialogContent,
  MatDialogActions,
  MatDialogClose,
} from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { FormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { DialogComponent } from './dialog/dialog.component';
declare const google: any;
export interface DialogData {
  animal: string;
  name: string;
}
@Component({
  selector: 'app-table-list',
  templateUrl: './table-list.component.html',
  styleUrls: ['./table-list.component.css']
})
export class TableListComponent implements OnInit, AfterViewInit {

  service;
  clients = []
  public reviewsSub = new BehaviorSubject([]);
  public reviews = []
  constructor(private profilService: ProfilService, public dialog: MatDialog) { }

  ngOnInit() {
    this.profilService.getClient().subscribe(
      (data) => {
        this.clients = data;
        console.log(this.clients); // Affichez les données dans la console
      },
      (error) => {
        console.error(error);
      }
    );
  }
  openDialog(client): void {
    this.dialog.open(DialogComponent, {
      data: { client: this.clients }, // Données à passer à la boîte de dialogue
    });
  }
  // openDialogBilling(client) {
  //   this.dialog.open(DialogComponent, {
  //     data: { client: this.clients }, // Données à passer à la boîte de dialogue
  //   });
  // }
  // openDialogContact(client) {
  //   this.dialog.open(DialogComponent, {
  //     data: { client: this.clients }, // Données à passer à la boîte de dialogue
  //   });
  // }
  ngAfterViewInit(): void {

    const request = {
      placeId: 'ChIJOZvBRUN1AhMRlz50m2oPHgA', // Remplacez par l'ID de votre lieu
      fields: ['reviews']
    };

    this.service = new google.maps.places.PlacesService(document.getElementById('googleReviews') as HTMLDivElement);

    this.service.getDetails(request, this.callback);

  }

  public callback = (place, status) => {
    if (status === google.maps.places.PlacesServiceStatus.OK) {
      this.reviewsSub.next(place.reviews.slice());
      this.reviews = this.reviewsSub.getValue()
      console.log("review", this.reviewsSub.getValue())
    }
  };
  createRange(number) {
    const items: number[] = [];
    for (let i = 1; i <= number; i++) {
      items.push(i);
    }
    return items;
  }

}