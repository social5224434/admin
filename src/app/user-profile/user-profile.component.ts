import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProfilService } from '../profil.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {
  clientForm: FormGroup
  constructor(private fb: FormBuilder, private service: ProfilService) { }

  ngOnInit() {
    this.clientForm = this.fb.group(
      {
        enseigneCommercial: ['a', Validators.required],
        companyAdress: ['a', Validators.required],
        companyWebsite: ['a', Validators.required],
        googleBusiness: ['a', Validators.required],
        googleStars: ['a', Validators.required],
        description: ['a', Validators.required],
        about_company: this.fb.group({
          activity: ['a', Validators.required],
          workForce: ['a', Validators.required],
          collaborationStartDate: ['a', Validators.required],
          history: ['a', Validators.required],
        }),
        contact: this.fb.group({
          fullName: ['a', Validators.required],
          telephone: ['5', Validators.required],
          email: ['a', Validators.required],
        }),
        billing_infos: this.fb.group({
          bankInfos: ['a', Validators.required],
          paymentTerms: ['a', Validators.required],
          paymentHistory: ['a', Validators.required],
        }),
        notes: ['a', Validators.required],
        additionNotes: ['a', Validators.required],

      }
    )

    

  }
  onSumbit() {
    console.log(this.clientForm.value);

    if (this.clientForm.valid) {
      this.service.ajouterClient(this.clientForm.value).subscribe(
        (res) => {
          console.log(res);

        },
        (error) => {
          console.log(error);
          // Gestion de l'erreur
        }
      )
    }
    else {

    }
  }

}
