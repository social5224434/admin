import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-typography',
  templateUrl: './typography.component.html',
  styleUrls: ['./typography.component.css']
})
export class TypographyComponent implements OnInit {
form: FormGroup;
  formArray: FormArray
  questionsArray=[
    {
      type:'input',
      question:'',
      
    },
    {
      type:'uniqueChoice',
      question:'',
      responses:['A','B','C']

    },
    {
      type:'multipleChoice',
      question:'',
      responses:['A','B','C']
    }

  ]
  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.form = this.fb.group({
      // Define your static form controls here
      name: ['', Validators.required],
      type: [''],
      questions : this.fb.array([])
    });

   
  }

  
  
  removeField(index: number) {
    this.formArray.removeAt(index);
  }
trackById(index: number): number {
  return index; // Assuming each item has a unique 'id' property
}
}
